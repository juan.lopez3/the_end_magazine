for d in sites/default/files
do
  sudo find $d -type d -exec chmod 770 {} \;
  sudo find $d -type f -exec chmod 644 {} \;
done
