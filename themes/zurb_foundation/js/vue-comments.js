var app = angular.module('app', [])
app.controller ('mainController', function($scope){
    $scope.newComment = "";
    $scope.mensaje = "hola";
    $scope.comments = [{

            autor: 'Jimena Martin',
            mensaje: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum arcu mi, eu sagittis turpis commodo quis. Sed interdum efficitur sapien sodales...',
            fecha: '01/15/2017 3:11PM',
            categoria: 'Pelicula 1'
        },
        {
            autor: 'Jimena Martin',
            mensaje: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum arcu mi, eu sagittis turpis commodo quis. Sed interdum efficitur sapien sodales...',
            fecha: '01/05/2017 3:11PM',
            categoria: 'Noticia 1 - Pelicula 1'            
        }]
    $scope.comentar = function() {
        $scope.comments.push({
            autor: 'Jimena Martin',
            mensaje: $scope.newComment,
            fecha: new Date(),
            categoria: 'Review - Pelicula 1' 
        });
        $scope.newComment = "";
    }
});
