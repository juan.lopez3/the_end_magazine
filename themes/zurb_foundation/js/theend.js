'use strict';

(function ($) {

    var $document = $('body');

    //tabs pelicula
    function seccionesPelicula() {
        //menu items
        var menu = $document.find('ul.temp-menu');
        var menuItem = $(menu).find('li');
        var menuLink = $(menu).find('li a');

        $('.horizontal-tabs-list').remove();

        menuLink.on('click', function (e) {
            e.preventDefault();

            var link = this;
            var item = $(link).parent('li');
            var itemIndex = menuItem.index(item);

            menuItem.removeClass('active');
            $(item).addClass('active');

            activeTab(itemIndex);

        });

        function activeTab(index) {
            var tabs = $('.horizontal-tabs-panes details');

            tabs.addClass('horizontal-tab-hidden');
            $(tabs[index]).removeClass('horizontal-tab-hidden');
        }
    }

    seccionesPelicula();


    $('#loader').hide();
    jQuery.ajaxSetup({
        beforeSend: function () {
            $('#loader').show();
        },
        complete: function () {
            $('#loader').hide();
        },
        success: function () {
            $('#loader').hide();
        }
    });

    /**
     * Container esto te podria interesar
     */
    $('.field-item #container_node_related').wrapAll('<div class="section-interesar" />');
    $('.section-interesar').before('<h3 class="title-interesar"> También te podría interesar </h3>');

    /**
     * toogle menu mobile
     */
    $('#block-zurb-foundation-main-menu > h2').click(function () {
        $("#block-zurb-foundation-main-menu ul").toggleClass("active");
        $("body").toggleClass("menu-active");
    });
    
    /**
     * Contenedor para buscador y redes del home
     */
    //$('.path-frontpage .block-region-middle > section:nth-child(2)').addClass('home-redes-menu');
    $('.path-frontpage .block-region-middle-top > section>div>.field-type-text-with-summary .field-item .social-network-site>.social-site').closest('section').addClass('home-redes-menu');
    $('.Login-popup-link').closest('section').addClass('Login-popup-link-content');
    $('.search-block-form, .home-redes-menu, .Login-popup-link-content').wrapAll('<div class="contenedor_menu_home"></div>')


    //init background check
    if ($('body').hasClass("path-frontpage")) {
        document.addEventListener('DOMContentLoaded', function () {
            BackgroundCheck.init({
                targets: '.contenedor_menu_home'
            });
        });
    }


    // effect trailer show and hide
    $("#pelicula-anchor .field-node--field-trailer-principal").before("<div class='close_trailer'> X </div>");

    $("#pelicula-anchor .field-node--field-trailer-principal .field-item").click(function () {
        $(".field-node--field-trailer-principal").animate({
            left: "50%",
            opacity: 1
        }, 500);
        $('.close_trailer').animate({
            opacity: 1
        }, 700);
    });

    $("#pelicula-anchor .field-node--field-trailer-principal").click(function () {
        $(".field-node--field-trailer-principal").animate({
            left: "45%",
            opacity: 1
        }, 500);
        $('.close_trailer').animate({
            opacity: 1
        }, 700);
        $('#pelicula-anchor .field-node--field-trailer-principal').removeClass('showPlay');

    });

    $(".close_trailer").click(function () {
        $("#pelicula-anchor .field-node--field-trailer-principal").animate({
            left: "45px",
            opacity: 1
        }, 500);
        $('.close_trailer').animate({
            opacity: 0
        }, 500);
        $('#pelicula-anchor .field-node--field-trailer-principal').addClass('showPlay');
    });

    /* end */


    $(".view-videos").closest('section').addClass('container-videos-home');

    // top ranking content
    $(".top-ranking-home-container").closest('section').addClass('top-ranking-container');
    $(".noticias-al-lado-de-ranking").closest('section').addClass('top-ranking-noticias');
    $('.top-ranking-container, .top-ranking-noticias').removeClass('views-element-container');
    $('.top-ranking-container, .top-ranking-noticias').wrapAll('<section class="contenedor_top_ranking views-element-container"></section>')


    $('.close_comments').click(function () {
        $('body').toggleClass("show-comments");
    });


    // menufestivales container
    $(".menu-fest .title-hora-fest,.menu-fest .menu_festival").wrapAll('<section class="menu-fes"></section>');

    // menu home container
    $("body.path-frontpage ul.navbar-top-links").addClass('item-container-menu-home');
    $("body.path-frontpage .contenedor_menu_home").addClass('item-container-menu-home');
    $("body.path-frontpage .field-block-content--body .social-network-site").closest('section').addClass('item-container-menu-home');
    $("body.path-frontpage .slider-home-main").closest('section').removeClass('item-container-menu-home');
    $("body.path-frontpage #block-socialnetworksite").removeClass('item-container-menu-home');
    $('.item-container-menu-home').wrapAll('<section class="big-container-menu"></section>');

    $(".dropdown a.dropdown-toggle").removeAttr("href");

    $('.dropdown.topbar-user.pull-right a.dropdown-toggle').on('click', function () {
        $('.dropdown .dropdown-menu').slideToggle(500);
    });


    //menu lateral
    $('#block-zurb-foundation-main-menu .block-title').on('click', function () {
        console.log('add class');
        $('#block-zurb-foundation-main-menu').removeClass('close');
        $('#block-zurb-foundation-main-menu').addClass('open');
        if ($('#block-zurb-foundation-main-menu').hasClass('open')) {
            console.log('ok');
            var delay = 0;
            $("ul.dropdown.menu li").each(function (index) {
                $(this).delay(delay).animate({
                    opacity: 1,
                    top: "5px"
                }, 100);
                delay += 250;
            });
            $('ul.dropdown.menu.active li:nth-child(1)').on('click', function () {
                $('#block-zurb-foundation-main-menu').removeClass('open');
                $('#block-zurb-foundation-main-menu').addClass('close');
                $("ul.dropdown.menu li").each(function (index) {
                    $(this).animate({
                        opacity: 0,
                        top: '-50px'
                    });
                });
            });
        }
    });

    // show sintax form
    $('.errores-ortografia').css('display', 'none');
    $('.field-node--dynamic-block-fieldnode-syntax-error .text-syntax-error').on('click', function () {
        $('.errores-ortografia').toggle('swing');
    });



    // menu sticky
    $('.block-zurb-foundation-content > div').stickem({
        item: '.menu-peli',
        container: '.block-zurb-foundation-content > div',
        offset: 100,
        start: 100
    });

    // $('.block-zurb-foundation-content > div').stickem({
    //     item: '.user-container-interna',
    //     container: '.block-zurb-foundation-content > div',
    //     offset: 350,
    //     start: 0  
    // });





})(jQuery);