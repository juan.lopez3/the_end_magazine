(function () {

  function getUrlFromImages (arr) {
    var foo = [];
    var url;

    arr.forEach(function (value) {
      url = value.querySelector('img').getAttribute('src');
      foo.push(url);
    })

    return foo;
  }

  function addBackgroundStyle (imagenesArticle, imagesUrl) {
    imagenesArticle.forEach(function (value, index) {
      value.style.backgroundImage = 'url(' + imagesUrl[index] + ')';
      console.log(value.style.backgroundImage);
    });
  }

  function init () {
    var $bodyArticle = document.querySelector('.field-node--body');
    var imagenesArticle = $bodyArticle.querySelectorAll('figure.align-center');
    var imagesUrl = [];

    //if(!imagenesArticle.lenght) { return; }

    imagesUrl = getUrlFromImages(imagenesArticle);

    addBackgroundStyle(imagenesArticle, imagesUrl);

  }

  init();
})();
