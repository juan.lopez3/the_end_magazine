'use strict';

(function ($) {

  /* Slider casting interna pelcicula */
  $(".cast-container").find(".field-name-field-personas-que-pertenecen-a->.field-items").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });

  /*landing editores*/
  $(".editores-landing").slick({
    centerMode: false,
    slidesToShow: 3,
    infinite: false,
    slide: '.views-row',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 0,
          slidesToScroll: 0,
          centerMode: false,
        }
      }
    ]
  });

  //listado todos los editores
  $('.editores-landing-totales-usuarios').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    arrows: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          dots: true,
          arrows: false
        }
      }
    ]
  });

  // siguiendo editores interna usuarios
  $('.siguiendo-usuarios').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          dots: true,
          arrows: false
        }
      }
    ]
  });

  // peliculas favoritas internas articulos
  $('.field-user--dynamic-block-fielduser-peliculas-favoritas .views-row').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          dots: true,
          arrows: false
        }
      }
    ]
  });


  // efecto imagen slide landing editores
  $(".editores-landing .slick-slide").each(function () {
    $(this).find('.field-user--user-picture img').after($(this).find('img').clone());
  });
  // efecto interna editor
  //$(".path-user .group-left .field-user--user-picture img").after($('.path-user .group-left .field-user--user-picture img').clone());


  /* Add class wrapping rec home widget elements */
  $('.editores-landing-totales-usuarios').addClass('editores-landing');
  $('.editores-title').closest('section').addClass('slide-editores');
  $('.editores-landing').closest('.views-element-container').addClass("slide-editores");
  $('.editores-landing').closest('.views-element-container').removeClass("views-element-container");
  $('.slide-editores').wrapAll('<section class="editores-lading-container views-element-container" />');

  //galeria
  $('.galeria-home .views-row').closest('section').addClass('container-galeria');
  $('.galeria-home .views-row').wrapAll('<section class="content-image-galeria" />');
  $('.content-image-galeria').slick({
    slidesToScroll: 1,
    arrows: true,
    centerPadding: '180px',
    slidesToShow: 2,
    centerMode: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerMode: false
        }
      }
    ]
  });


  // slick by path-user
  if ($('body').hasClass('path-user')) {
    //slider interna editor
    $('.slider-popular-post .items').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
          }
        }
      ]
    });
  }


  $('.video-grid-container').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    slide: '.views-row',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          slidesToShow: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });

  $('.podria-interesar ul').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      }
    ]
  });


})(jQuery);
