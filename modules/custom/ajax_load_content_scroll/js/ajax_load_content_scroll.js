(function ($) {
    'use strict';
    Drupal.behaviors.ajax_load_content_scroll = {
        attach: function (context, settings) {

            // Ocultar todos los menus de las pelis
            $(".node--type-pelicula").each(function () {
                var distancia = $(this).offset().top;
                if ($(window).scrollTop() >= distancia) {
                    //$('.menu-peli').hide();
                    //$(this).find('.menu-peli').show();
                }
            });

            /**
             * Permite poner el título dentro de la url al compartir un contenido
             */
            var socialItem = $(".social-media-sharing");
            var title = socialItem.closest('.group-left').find('.titles-review').find('h2').text();
            if (title && socialItem) {
                socialItem.find('ul li').each(function () {
                    var url = $(this).find('a').attr('href');
                    if (url) {
                        url = url.replace("[comment:parent:node:menu-link:title]", title);
                        url = url.replace("Panel de películas", title);

                        $(this).find('a').attr('href', url);
                    }
                });
            }


            var array = [{
                "id_container": "container-content-review",
                "id_campo": "field_reviews_peli",
                "id_full_content": "review-content-full"
            },
                {
                    "id_container": "container-content-resumen",
                    "id_campo": "field_resumen_review",
                    "id_full_content": "resumen-content-full"
                },
                {
                    "id_container": "container-content-noticias",
                    "id_campo": "field_noticias_peli",
                    "id_full_content": "noticia-content-full"
                },
                {
                    "id_container": "container-content-videos",
                    "id_campo": "field_videos",
                    "id_full_content": "video-content-full"
                },
                {
                    "id_container": "container-content-imagenes",
                    "id_campo": "field_imagenes_peli",
                    "id_full_content": "imagenes-content-full"
                }
            ];

            for (var i = 0; i < array.length; i++) {
                var id_container = array[i]["id_container"];
                //                $('#' + id_container).hide();
            }


            $(window).scroll(function (event) {

                $(".node--type-pelicula").each(function () {
                    var distancia = $(this).offset().top;
                    var altura = $(this).height();
                    if ($(window).scrollTop() >= distancia) {
                        //$('.menu-peli').hide();
                        //$('.menu-peli').removeClass('sticky');
                        //$(this).find('.menu-peli').show();
                        //$(this).addClass('sticky');
                    } else {
                        //$(this).removeClass('sticky');
                    }
                });


            });

            function callback_ajax(id_pelicula, id_campo, id_container, id_full_content) {

                var url = settings.path.baseUrl + "/get_content/" + id_pelicula + "/" + id_campo;
                $.ajax({
                    method: "POST",
                    url: url,
                    dataType: "html",
                    async: false,
                    success: function (data) {

                        var result = $("#" + id_container + " ." + id_full_content).append(data).find('#block-zurb-foundation-content').html();
                        $("#" + id_container + " ." + id_full_content).html(result);

                        /**
                         * Validamos si la pelicula tiene mas de 12 videos
                         * si si se convierte a flexslider
                         * ya que si hay menos se muestran todos los videos cuando se carga la pagina
                         */
                        var numItems = $('.listado-videos ul.slides li.field-item .node--view-mode-teaser').length;

                        if (numItems > 12) {
                            $('.flexslider_videos .slides').slick({
                                infinite: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true,
                                centerPadding: '170px'
                            });
                        }

                        /**
                         * Cuando se haga click sobre un contenedor de video por medio de ajax se llama
                         * el despliegue full deel video y se inserta el markup en el div full-video
                         */
                        $("div.node--type-video.node--view-mode-teaser").on("click", function () {
                            /**
                             * Convertir listado de videos en slider
                             */
                            $('.flexslider_videos .slides').slick({
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                dots: true,
                                centerMode: false,
                                focusOnSelect: true,
                                responsive: [{
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        infinite: true
                                    }
                                }]
                            });


                        });

                        /**
                         * cerrar full video
                         * eliminamos el slider para que vuelva a quedar el grid
                         * desaparecemos el contenido del full video
                         */
                        $("span.cerrar_video").on("click", function () {
                            $('.flexslider_videos .slides').slick('unslick');
                            $(".full-video").html("");
                        });

                        /*slider imagenes interna pelicula */
                        $('.flexslider_imagenes .slides').slick({
                            infinite: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '170px',
                            responsive: [{
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    centerMode: false,
                                    centerPadding: '0px',
                                    arrows: false,
                                    dots: true
                                }
                            }]
                        });
                    },
                    error: function (xhr, status) {
                        $("#" + id_container + " ." + id_full_content).html("Sorry, there was a problem!");
                    },
                    beforeSend: function () {
                        $("#" + id_container + " ." + id_full_content).html("<div class='cargando_siguiente_articulo'>_CARGANDO SIGUIENTE ARTICULO</div>");
                    }
                });

            }
        }
    };
}(jQuery));
