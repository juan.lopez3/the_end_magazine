<?php

/**
 * @file
 * Contains Drupal\ajax_load_content_scroll\Controller\AjaxLoadContentScrollController
 */

namespace Drupal\ajax_load_content_scroll\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Render\Markup;

/**
 * Controller routines for AjaxLoadContentScrollController routes.
 */
class AjaxLoadContentScrollController extends ControllerBase {

    /**
     *
     * @param type $id_video
     * @return string
     */
    public function get_content($id_pelicula, $id_campo) {
        $node = Node::load($id_pelicula);

//        kpr($node->$id_campo);
        

        if ($node->$id_campo) {
//        $tipo = $node->$id_campo->getFieldDefinition()->getType();
//        kpr($tipo);


            $display_settings = [];
            switch ($id_campo) {
                case "field_reviews_peli":
                    $display_settings = [
                        'label' => 'hidden',
                        'type' => 'entity_reference_entity_view'
                    ];

                    break;

                case "field_resumen_review":

                    $display_settings = [
                        'label' => 'above',
                        'type' => 'text_long',
                    ];

                    break;

                case "field_noticias_peli":
                    $display_settings = [
                        'label' => 'hidden',
                        'type' => 'entity_reference_entity_view',
                    ];

                    break;
                case "field_videos":
                    $display_settings = [
                        'label' => 'hidden',
                        'type' => 'entity_reference_entity_view',
                        'settings' => ['view_mode' => 'teaser']
                    ];
                    break;

                case "field_imagenes_peli":
                    $display_settings = [
                        'label' => 'hidden',
                        'type' => 'field_collection_items',
                    ];

                    break;
            }

            $output = $node->$id_campo->view($display_settings);

//            
//            echo render($output)->__toString();

            return ["#markup" => render($output)->__toString()];
        }

        return ["#markup" => "<p>Error</p>"];
    }

}
