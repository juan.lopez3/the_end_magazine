<?php

namespace Drupal\get_last_node_per_user\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\node\Entity\Node;

//Views

/**
 * Plugin that renders the field.
 *
 * @DsField(
 *   id = "last_node_by_user",
 *   title = @Translation("Último nodo creado"),
 *   entity_type = "user",
 *   provider = "get_last_node_per_user",
 *
 * )
 */
class LastNodeCreated extends DsFieldBase {
	/**
	 * {@inheritdoc}
	 */
	public function build() {

		$user = $this->entity();
		$lastNode = $this->getLastNodeByUser($user);

		if (!is_null($lastNode)) {
			$title = $lastNode->get('title')->value;
			$options = ['absolute' => TRUE];
			$url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $lastNode->nid->value], $options);
			$url = $url->toString();
			$print = '<div><div><h2>último artículo</h2></div><a href="' . $url . '"> ' . $title . '</a></div>';
		} else {
			$title = 'No hay artículos aún';
			$print = '<div><div><h2>último artículo</h2></div>' . $title . ' </div>';

		}


		//Permite poner campo para cargar el siguiente nodo
		return array(
			'#markup' => $print
		);
	}

	private function getLastNodeByUser($user) {
		$nid = NULL;
		$allowed = ['articulo', 'review'];
		$query = \Drupal::entityQuery('node')
			->condition('status', 1)
			->condition('type', $allowed, 'IN')
			->condition('uid', $user->uid->value, '=')
			->range(0, 1);

		$nids = $query->execute();
		if ($nids) {
			$nid = reset($nids);
			$nid = Node::load($nid);
		}
		return $nid;
	}
}
