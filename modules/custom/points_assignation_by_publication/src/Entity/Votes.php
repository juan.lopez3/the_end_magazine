<?php

/**
 * Class Votes
 */
class Votes {

	private $value_per_submission;
	private $value_per_early_date;
	private $value_per_late_date;
	private $value_per_mistakes;

	/**
	 * Votes constructor.
	 * @param $value_per_submission
	 * @param $value_per_early_date
	 * @param $value_per_late_date
	 * @param $value_per_mistakes
	 */
	public function __construct($value_per_submission, $value_per_early_date, $value_per_late_date, $value_per_mistakes) {
		$this->value_per_submission = $value_per_submission;
		$this->value_per_early_date = $value_per_early_date;
		$this->value_per_late_date = $value_per_late_date;
		$this->value_per_mistakes = $value_per_mistakes;
	}

	/**
	 * @return mixed
	 */
	public function getValuePerSubmission() {
		return $this->value_per_submission;
	}

	/**
	 * @param mixed $value_per_submission
	 */
	public function setValuePerSubmission($value_per_submission) {
		$this->value_per_submission = $value_per_submission;
	}

	/**
	 * @return mixed
	 */
	public function getValuePerEarlyDate() {
		return $this->value_per_early_date;
	}

	/**
	 * @param mixed $value_per_early_date
	 */
	public function setValuePerEarlyDate($value_per_early_date) {
		$this->value_per_early_date = $value_per_early_date;
	}

	/**
	 * @return mixed
	 */
	public function getValuePerLateDate() {
		return $this->value_per_late_date;
	}

	/**
	 * @param mixed $value_per_late_date
	 */
	public function setValuePerLateDate($value_per_late_date) {
		$this->value_per_late_date = $value_per_late_date;
	}

	/**
	 * @return mixed
	 */
	public function getValuePerMistakes() {
		return $this->value_per_mistakes;
	}

	/**
	 * @param mixed $value_per_mistakes
	 */
	public function setValuePerMistakes($value_per_mistakes) {
		$this->value_per_mistakes = $value_per_mistakes;
	}

	/**
	 * @return array
	 */
	public function getFormState() {
		return $this->form_state;
	}

	/**
	 * @param array $form_state
	 */
	public function setFormState($form_state) {
		$this->form_state = $form_state;
	}

}