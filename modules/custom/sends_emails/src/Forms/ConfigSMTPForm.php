<?php

/**
 * @file
 * Contains Drupal\sends_emails\Forms\ConfigSMTPForm.
 */

namespace Drupal\sends_emails\Forms;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an sends_emails form.
 */
class ConfigSMTPForm extends FormBase {

    /**
     * 
     * @return string
     */
    public function getFormId() {
        return 'sends_emails_form';
    }

    /**
     *
     * @param array $form
     * @param FormStateInterface $form_state
     * @param type $nombre - (encargados - empleados)
     * @return type
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

       
        $valores = \Drupal::state()->getMultiple(['host','username','password','port']);

        $form['host'] = [
            '#title' => "Host",
            '#type' => 'textfield',
            '#required' => TRUE,
            '#default_value' => $valores['host']
        ];

        $form['username'] = [
            '#title' => "Username",
            '#type' => 'textfield',
            '#required' => TRUE,
            '#default_value' => $valores['username']
        ];

        $form['password'] = [
            '#title' => "Password",
            '#type' => 'password',
            '#required' => TRUE,
            '#default_value' => $valores['password']
        ];

        $form['port'] = [
            '#title' => "Port",
            '#type' => 'textfield',
            '#required' => TRUE,
            '#default_value' => $valores['port']
        ];

        $form['actions']['#type'] = 'actions';
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save'),
        ];
        return $form;
    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $host = $form_state->getValue("host");
        $username = $form_state->getValue("username");
        $password = $form_state->getValue("password");
        $port = $form_state->getValue("port");


        \Drupal::state()->set("host", $host);
        \Drupal::state()->set("username", $username);
        \Drupal::state()->set("password", $password);
        \Drupal::state()->set("port", $port);
        

        drupal_set_message(t('Configuración guardada.'));
    }

}
