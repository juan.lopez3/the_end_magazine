<?php

/**
 * @file
 * Contains Drupal\sends_emails\SendsEmails.
 */

namespace Drupal\sends_emails;

//$base = realpath(drupal_get_path("module", "sends_emails"));
//require_once($base . "/src/phpmailer/phpmailer/.php");

/**
 * Provides route responses for the Example module.
 */
class SendsEmails {

    /**
     * 
     * @param type $to
     * @param type $subject
     * @param type $body
     */
    public static function send_email_config($to, $subject, $body) {

        $params['subject'] = $subject;
        $params['message'] = $body;

        return self::send_email($to, $params);
    }

    /**
     * 
     * @param type $to
     * @param type $params
     * @return boolean
     */
    protected static function send_email($to, $params) {

        $subject = $params["subject"];
        $body = $params["message"];

        $valores = self::get_values_config_smtp();

        $mail = new \PHPMailer();

        $mail->IsSMTP();
        $mail->Host = $valores["port"];

        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->Host = $valores["host"];
        $mail->Port = $valores["port"];
        $mail->Username = $valores["username"];
        $mail->Password = $valores["password"];

        $mail->SetFrom($valores["username"], 'ProtecData');

        $mail->Subject = utf8_decode($subject);
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
        $mail->MsgHTML($body);

        $address = $to;
        $mail->AddAddress($address, $address);

        if (isset($params['files']))
            $mail->AddAttachment(drupal_realpath($p['files']));

        if (!$mail->Send()) {
            \Drupal::logger('email')->error("Error al enviar correo a " . $to);
            return FALSE;
        } else {
            \Drupal::logger('email')->notice("Correo enviado a " . $to);
            return TRUE;
        }
    }

    /**
     * 
     * @return type
     */
    protected static function get_values_config_smtp() {
        return \Drupal::state()->getMultiple(['host', 'username', 'password', 'port']);
    }

}
