<?php
/**
 * @file
 * Contains \Drupal\footer\Plugin\Block\BlockFooter.
 */

namespace Drupal\footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'footer' block.
 *
 * @Block(
 *   id = "footer_site",
 *   admin_label = @Translation("Footer site"),
 *
 * )
 */
class BlockFooter extends BlockBase {
	/**
	 * {@inheritdoc}
	 */
	public function build() {

		// Load the configuration from the form
		$config = $this->getConfiguration();
		$test_value = isset($config['test']) ? $config['test'] : '';

		$build = [];
		$build['#theme'] = 'footer_site';

		// You would not do both of these things...
		$build['#test_value'] = $test_value;
		$build['module_block_test']['#markup'] = '<p>' . $test_value . '</p>';

		return $build;



	}
}