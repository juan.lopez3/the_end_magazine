jQuery(document).ready(function () {

    $ = jQuery;
    $(".views-infinite-scroll-content-wrapper").find(".row-festivales.views-row").each(function () {
        var id = $(this).find(".node--type-festival").data("history-node-id") || 0;

        $(this).attr("id", id);

    });


    jQuery(document).ajaxComplete(function () {

        $ = jQuery;
        $(".views-infinite-scroll-content-wrapper").find(".row-festivales.views-row").each(function () {
            var id = $(this).find(".node--type-festival").data("history-node-id") || 0;

            $(this).attr("id", id);

        });

    });

});
