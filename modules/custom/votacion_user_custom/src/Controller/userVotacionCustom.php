<?php

namespace Drupal\votacion_user_custom\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\user\Entity\User;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\field_collection\Entity\FieldCollectionItem;

/**
 * Class userVotacionCustom
 * @package Drupal\votacion_user_custom\Controller
 */
class userVotacionCustom extends ControllerBase {

	/**
	 * @method Save the values given for the user in editor profile
	 * @param {int} $uid
	 * @param {int} $val
	 * @return JsonResponse
	 */
	public function userVotacion($uid, $val) {
		$user = User::load($uid);
		$val = (int)$val;
		$value = ($user->get('field_votacion_custom_value')->value !== null) ? $user->get('field_votacion_custom_value')->value : 0;
		$cont = ($user->get('field_votacion_custom_count')->value !== null) ? $user->get('field_votacion_custom_count')->value : 0;
		$cont = $cont + 1;
		$value = number_format(($value + $val) / $cont, 1);
		$user->set('field_votacion_custom_value', $value);
		$user->set('field_votacion_custom_count', $cont);
		$user->save();
		return new JsonResponse(array('content' => TRUE));
	}
}