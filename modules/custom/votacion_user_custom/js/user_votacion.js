(function () {
    $ = jQuery

    var url = window.location.href.split('/peliculas')[0];     // Returns full URL


    $("body").on("mouseover", ".votacion-custom li", function () {
        var idhovered = $(this).attr("id");

        var item = '<div class="item-hovered"> <span>' +
            idhovered
        '</span></div>';
        $(this).parents(".votacion-custom").find(".item-hovered").remove();
        $(this).parents(".votacion-custom").append(item);

    });


    $("body").on("click", "#user_vite_review_custom ul li", function () {
        $('#user_vite_review_custom ul li').fadeOut(500);
        $('#user_vite_review_custom p').fadeOut(500);
        //$(".votacion-custom").find(".item-hovered").fadeOut(500);
        var idclicked = $(this).attr("id");
        var userid = $(this).parents('ul').data('user-id');
        console.log("Le va a dar al usuario ", userid, " el valor de ", idclicked);
        var status = save_vote_given(userid, idclicked, url);
        if (status) {
            //Display fine
        } else {
            //Display error
        }
    });


    function save_vote_given(uid, val, url) {
        var promise = $.ajax({
            url: Drupal.url("") + '/votacion_user_custom_save/' + uid + '/' + val
        });

        promise
            .then(function (data) {

                //El dato fue guardado
                console.log(data.content);
                return data.content;

            }, function (error) {
                console.log(error);
                return false;
            });
    }

})();