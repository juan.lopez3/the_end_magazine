/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/////////////
/////////////
/////////////
/////////////YOUTUBE
/////////////
/////////////

jQuery(document).ready(function () {

    var playerInfoList = [];
    var id_video = '';
    var players = new Array();
    var img;

    jQuery("div.field-node--field-trailer-principal.field-name-field-trailer-principal.field-type-youtube").on("click", function (i) {
        img = jQuery(this).find("img");
        if(img.length === 1){
            var splitimg1 = img.attr("src").split(".");
            var splitimg2 = splitimg1[0].split("/");
            id_video = (splitimg2[splitimg2.length-1]) ? splitimg2[splitimg2.length-1] : '';

            var width = img.width();
            var height = img.height();


            playerInfoList.push([{
                videoId: id_video,
                height: height,
                width: width
            }]);
        }
    });
    var cargado_libreria = false;

    jQuery("div.field-node--field-trailer-principal.field-name-field-trailer-principal.field-type-youtube").click(function () {
        var info = playerInfoList[0][0];

        if (cargado_libreria != true) {
            // 2. This code loads the IFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            cargado_libreria = true;
        }


        jQuery(this).html("<div id='player_" + info.videoId + "'></div>");

        videoPlayer(info.videoId);

    });

    function videoPlayer(id_video) {

        var info = playerInfoList[0][0];


        //hide and show divs
        var img = jQuery('#' + info.videoId + ' img');
        var width = info.width;
        var height = info.height;
        //jQuery('#' + info.videoId + ' .imagen').hide().width(width).height(height);

        // 3. This function creates an <iframe> (and YouTube player) after the API code downloads.
        var player;


        if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
            window.onYouTubePlayerAPIReady = function () {
                onYouTubePlayer();
            };
        } else {
            onYouTubePlayer();
        }



        function onYouTubePlayer() {
            if (typeof playerInfoList === 'undefined')
                return;

            createPlayer(info);
        }

        function createPlayer(playerInfo) {
            var info = playerInfoList[0][0];

            //Creates an iframe n remove the 'player_' + info.videoId div
             player = new YT.Player('player_' + info.videoId, {
             width: info.width,
             height: info.height,
             videoId: info.videoId,
             playerVars: {'showinfo': 0},
             events: {
             'onReady': onPlayerReady,
             'onStateChange': onPlayerStateChange
             }
             });

            players.push(player);
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
                event.target.playVideo();
        }

        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING) {
                //alert(event.target.getVideoUrl());
                // alert(players[0].getVideoUrl());
                var temp = event.target.getVideoUrl();
                for (var i = 0; i < players.length; i++) {
                    if (players[i].getVideoUrl() != temp) {
                        players[i].stopVideo();
                    }
                }
            }
        }

    }
});
