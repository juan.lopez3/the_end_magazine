<?php

/**
 * Drupal\ajax_load_user_home\Controller
 */
namespace Drupal\ajax_load_user_home\Controller;

/**
 * @import user, controllerBase, 
 */
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AjaxLoadUserHome
 * @package Drupal\ajax_load_user_home\Controller
 */
class AjaxLoadUserHome extends ControllerBase {

	/**
	 * @param $uid  the user id
	 * @return array userDS rendered
	 */
	public function get_user_content($uid) {

		if (!$uid)
			return new JsonResponse(array('content' => 'No se han podido cargar los datos'));

		$user = User::load($uid);
		//load user ds
		$view_builder = \Drupal::entityManager()->getViewBuilder('user');
//		$user = render($view_builder->view($user));


		$d = \Drupal::service('renderer')->renderPlain($view_builder->view($user));

		return new JsonResponse(array('content' => htmlentities($d->__toString())));


		//return ["#markup" => $user];
	}

}
