/**
 * @file
 */
(function () {
    $ = jQuery;
    var bandera=0;
    $(document).ready(function () {
        var userid = '';
        $('.slick-track').find('.slick-slide').on('click', function (e) {
            var userid = $(this).find("div").data('quickedit-entity-id').split('/')[1];
            if (!userid)
                return false;

            var promise = $.ajax({
                url: 'get_user_content_home/' + userid
            });

            promise
                .then(function (data) {
                    var elemento=$( ".contenedor_top_ranking" );
                    var elementoD=$( ".editores-landing-totales-usuarios");
                    $("#div-tmp-contaner-editor").remove();
                    if(bandera==0){
                        $(elemento).prepend("<div id='editInfo' style='margin-bottom: 50px;'></div>");
                        var data = $("<div/>").html(data.content).text();
                        $("#editInfo").append(data);
                    }
                    else{
                        $("#editInfo").html("");
                        var data = $("<div/>").html(data.content).text();
                        $("#editInfo").append(data);
                    }

                    var pix=elementoD.offset().top;
                    $("html, body").animate({scrollTop:(pix+180)+"px"});
                    elemento.show( "drop");

                    //slider interna editor
                    $('.slider-popular-post .items').slick({
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: true,
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 2,
                                }
                            }
                        ]
                    });
                 bandera++;

                }, function (error) {
                });


        });
    });


})();