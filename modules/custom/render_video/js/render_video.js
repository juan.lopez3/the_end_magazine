    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/////////////
/////////////
/////////////
/////////////YOUTUBE
/////////////
/////////////

jQuery(document).ready(function () {

    console.log("  Carga render_video  ");

    var playerInfoList = [];

    var players = new Array();


    /**
     * Agrega el atributo a la imagen para poder llamar los vídeos de youtube
     */

    jQuery(".field-node--nodos-relacionados-pelicula .field-node--field-link-youtube").each(function (i) {
        var src = $(this).find("img").attr("src");
        if (!src) {

        } else {


            var splitted = src.split("/");

            var dirtyId = splitted[splitted.length - 1];
            var vid = '';
            if (dirtyId.includes(".jpg")) {
                vid = dirtyId.split(".jpg")[0];
            } else {
                vid = dirtyId.split(".")[0];
            }
            $(this).find("img").attr("data-id-yt", vid);
        }
    });


    //jQuery("body").on("each", "#video-content-full ul.slides li", function (i) {
    jQuery(document).ajaxComplete(function () {
        jQuery(".field-node--nodos-relacionados-pelicula .field-node--field-link-youtube").each(function (i) {
                var src = $(this).find("img").attr("src");
                if (!src) {

                } else {


                    var splitted = src.split("/");

                    var dirtyId = splitted[splitted.length - 1];
                    var vid = '';
                    if (dirtyId.includes(".jpg")) {
                        vid = dirtyId.split(".jpg")[0];
                    } else {
                        vid = dirtyId.split(".")[0];
                    }
                    $(this).find("img").attr("data-id-yt", vid);
                }
            }
        );
    });

    var cargado_libreria = false;

    //jQuery("ul.slides").find("li").find("img").click(function () {
    jQuery("body").on("click", ".field-node--nodos-relacionados-pelicula .field-node--field-link-youtube img", function () {
        var id_video = jQuery(this).attr("data-id-yt");
        $(this).addClass("VideoActive");
        if (cargado_libreria != true) {
            // 2. This code loads the IFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            cargado_libreria = true;
        }


        $(this).parents(".field-node--nodos-relacionados-pelicula .field-node--field-link-youtube").prepend("<div id='player_" + id_video + "'></div>");

        videoPlayer(id_video, $(this));

    });

    function videoPlayer(id_video, img) {

        var info = playerInfoList[id_video];

        //hide and show divs
        var img = img;
        var width = img.width() * 2;
        var height = img.height() * 2;
        img.hide().width(width).height(height);

        var info = {
            width: width,
            height: height,
            videoId: id_video,
        };

        // 3. This function creates an <iframe> (and YouTube player) after the API code downloads.
        var player;


        if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
            window.onYouTubePlayerAPIReady = function () {
                onYouTubePlayer();
            };
        } else {
            onYouTubePlayer();
        }

        function onYouTubePlayer() {
            if (typeof playerInfoList === 'undefined')
                return;

            createPlayer(info);
        }

        function createPlayer(playerInfo) {
            var info = playerInfo;

            player = new YT.Player('player_' + info.videoId, {
                width: info.width,
                height: info.height,
                videoId: info.videoId,
                playerVars: {'showinfo': 0},
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });

            players.push(player);
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING) {
                //alert(event.target.getVideoUrl());
                // alert(players[0].getVideoUrl());
                var temp = event.target.getVideoUrl();
                for (var i = 0; i < players.length; i++) {
                    if (players[i].getVideoUrl() != temp) {
                        players[i].stopVideo();
                    }
                }
            }
        }

    }
})
;
