<?php

namespace Drupal\render_video\Plugin\Field\FieldFormatter;

// FormatterBase class.
use Drupal\Core\Field\FormatterBase;
// FieldItemInterface
use Drupal\Core\Field\FieldItemListInterface;

define('API_KEY', 'AIzaSyBPPWgqN4LHm7PZ3f6E9amZH5Wez9nU3cQ');

/**
 * Plugin implementation of the 'render_video' formatter.
 *
 * @todo https://www.drupal.org/node/1829202 Merge into 'link' formatter once
 *   there is a #type like 'item' that can render a compound label and content
 *   outside of a form context.
 *
 * @FieldFormatter(
 *   id = "render_video",
 *   label = @Translation("Video Youtube"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class RenderVideoFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {

        $elements = array(); return $elements;

        if (!empty($items->getValue())) {

            $valor = $items->getValue();
            $valor = reset($valor); 
            $valor = $valor['uri'];

            $id_video = $this->get_id_video($valor);

            if (!is_null($id_video)) {
                $data = file_get_contents("https://www.googleapis.com/youtube/v3/videos?key=" . API_KEY . "&part=snippet&id=" . $id_video);
                $json = json_decode($data);
                $url = $json->items[0]->snippet->thumbnails->high->url;
                $alt = $json->items[0]->snippet->title;

                $element = [
                    '#markup' => \Drupal::theme()->render('render_video', [
                        'url' => $url,
                        'alt' => $alt,
                        'id_video' => $id_video
                    ]),
                    '#attached' => [
                        'library' => [
                            'render_video/render_video'
                        ],
                    ],
                ];

                return $element;
            }
        }
    }

    /**
     * 
     * @param type $valor
     * @return type
     */
    private function get_id_video($valor) {
        $valor = explode("?v=", $valor);
        if (isset($valor[1])) {
            $valor = explode("&list=", $valor[1]);
            return $valor[0];
        }
        return NULL;
    }

}
