(function () {

  var coverFullScreen = document.querySelector('.node.node--view-mode-teaser.node--type-festivales .field-node--field-back-ground');
  var srcImagenCover = coverFullScreen.querySelector('img').getAttribute('src');

  coverFullScreen.style.backgroundImage = 'url('+ srcImagenCover +')';

})();
