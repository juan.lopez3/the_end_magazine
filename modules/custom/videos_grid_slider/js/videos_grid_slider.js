(function ($) {
    loadVideoGrid();
})(jQuery)

function loadVideoGrid() {

    var body = document.body
    var popUp = document.createElement('div')
    var container = document.createElement('div')
    var closePopUp = document.createElement('div')


    closePopUp.classList.add('closeEndPopUp')
    container.classList.add('container')

    popUp.classList.add('endPopUp')
    popUp.appendChild(container)
    popUp.appendChild(closePopUp)


    var videos = document.querySelectorAll('.video-grid-container .field-node--field-link-youtube')

    videos.forEach(function (video) {
        video.addEventListener('click', function (e) {

          console.log('video clicked', e)
            //Si tiene un iframe
            // Quitar los campos


            body.appendChild(popUp)
            body.classList.add('endPopUpActive')
            popUp.classList.add('active')
        })
    })

    function handlerClosePopUp(e) {
        jQuery('div.endPopUp').find(".container").empty();
        //body.classList.add('endPopUpTransitioning')
        body.classList.remove('endPopUpActive')
        popUp.classList.remove('active')
    }

    closePopUp.addEventListener('click', handlerClosePopUp)


    jQuery(document).ready(function () {

        var playerInfoList = [];

        var players = new Array();

        //Assing youtube id as data-id in image
        $(".video-grid-container").find(".views-row").each(function () {
            var img = $(this).find(".group-left").find('img');
            var splitted = img.attr('src').split("/");
            var dirtyId = splitted[splitted.length - 1];
            var vid = '';
            if (dirtyId.includes(".jpg")) {
                vid = dirtyId.split(".jpg")[0];
            } else if (dirtyId.includes(".png")) {
                vid = dirtyId.split(".png")[0];
            } else {
                vid = dirtyId.split(".")[0];
            }
            img.attr('data-id-yt', vid);
            img.attr('data-cont-id', 'contenedor_' + vid);
            img.parents(".views-row").attr('id', 'contenedor_' + vid);
        });


        $(".video-grid-container").find(".views-row").each(function () {
            var img = $(this).find(".group-left").find('img');
            var id_video = img.attr("data-id-yt");
            //var cont_id = jQuery(this).attr("id");
            var cont_id = img.data('cont-id');

            //var img = jQuery('#' + cont_id + ' img');

            var width = img.width();
            var height = img.height();

            playerInfoList[id_video] = [{
                id: cont_id,
                videoId: id_video,
                height: height,
                width: width
            }];

        });

        var cargado_libreria = false;

        $(".video-grid-container").find(".views-row").find(".group-left").find('img').click(function () {
            jQuery('div.endPopUp').find(".container").empty();
            var self = jQuery(this)
            setTimeout(function () {
                var id_video = self.attr("data-id-yt");

                if (cargado_libreria != true) {
                    // 2. This code loads the IFrame Player API code asynchronously.
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                    cargado_libreria = true;
                    console.log("se agrego libreria");
                }


                var title = self.parents('.views-row').find('.field-node--node-title').find('h2').clone();
                var desc = self.parents('.views-row').find('.field-node--body').find('.field-item').clone();
                var slider = jQuery(".video-grid-container").clone();
                //Poner el vídeo dentro del popup
                jQuery('div.endPopUp').find('.container').append("<div class='container-player-popup' id='player_" + id_video + "'></div>");
                //Poner el título dentro del popup
                jQuery('div.endPopUp').find('.container').append(title);
                //Poner la descripción dentro del popup
                jQuery('div.endPopUp').find('.container').append(desc);

                videoPlayer(id_video);
            }, 10);


        });

        function videoPlayer(id_video) {

            var info = playerInfoList[id_video];
            //hide and show divs
            var img = jQuery('#' + info[0].id + ' img');
            var width = img.width();
            var height = img.height();
            //jQuery('#' + info[0].id + ' .imagen').hide().width(width).height(height);

            // 3. This function creates an <iframe> (and YouTube player) after the API code downloads.
            var player;


            if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
                window.onYouTubePlayerAPIReady = function () {
                    onYouTubePlayer();
                };
            } else {
                onYouTubePlayer();
            }

            function onYouTubePlayer() {
                if (typeof playerInfoList === 'undefined')
                    return;
                createPlayer(info);
            }

            function createPlayer(playerInfo) {
                var info = playerInfo[0];

                player = new YT.Player('player_' + info.videoId, {
                    width: info.width,
                    height: info.height,
                    videoId: info.videoId,
                    playerVars: {'showinfo': 0},
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });

                players.push(player);
            }

            // 4. The API will call this function when the video player is ready.
            function onPlayerReady(event) {
                event.target.playVideo();
            }

            function onPlayerStateChange(event) {

                //video finalizado
                if (event.data === 0) {
                    handlerClosePopUp();
                    return;
                }

                if (event.data == YT.PlayerState.PLAYING) {
                    //alert(event.target.getVideoUrl());
                    // alert(players[0].getVideoUrl());
                    var temp = event.target.getVideoUrl();
                    for (var i = 0; i < players.length; i++) {
                        if (players[i].getVideoUrl() != temp) {
                            players[i].stopVideo();
                        }
                    }
                }
            }

        }
    });


}
