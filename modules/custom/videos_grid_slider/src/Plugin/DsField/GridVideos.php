<?php

namespace Drupal\videos_grid_slider\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;

//Views
/**
 * Plugin that renders the field.
 *
 * @DsField(
 *   id = "videos_grid_peli_list",
 *   title = @Translation("Grid videos relacionados película"),
 *   entity_type = "node",
 *   provider = "videos_grid_slider",
 *
 * )
 */
class GridVideos extends DsFieldBase {
	/**
	 * {@inheritdoc}
	 */
	public function build() {

		$node = $this->entity();

		//Cargar los argumentos a pasar a la vista
		$args = $node->nid->value;

		$display_id = 'block_1';
		$output = views_embed_view('videos_grid_pelicula', $display_id, $args);

		$node = \Drupal\node\Entity\Node::load($node->get('nid')->value);
		//Asignar la vista al campo


		return array(
			'#markup' => render($output)
		);
	}
}