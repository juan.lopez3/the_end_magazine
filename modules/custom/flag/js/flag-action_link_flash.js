/**
 * @file
 * Defines the client side code for the ActionLinkFlashCommand.
 *
 * see \Drupal\flag\Ajax\ActionLinkFlashCommand
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.flagAttach = {
    attach: function (context, settings) {
      // Bind a click handler onto all action links so styles can be applied
      // while the XHR is in flight.
      $(context).find('.flag a').each(function (i, element) {
        $(this).on('click', function () {
          $(this).parents().first().addClass('flag-waiting');
        });
      });
    }
  };

  /**
   * Display message on the screen for a short period of time.
   *
   * @param {Drupal.Ajax} [ajax]
   *   The ajax object.
   * @param {object} response
   *   Object holding the server response.
   * @param {string} response.selector
   *   The selector of link to be updated.
   * @param {string} response.message
   *   The message to be displayed.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.actionLinkFlash = function (ajax, response, status) {
    if (status === 'success') {
      // Construct a <p> element with escaped message.
      var p_elem = $('<p class="js-flag-message">').text(response.message);

      $(response.selector).append(p_elem);

      p_elem.fadeIn('fast', function () {
        $(this).fadeOut(15000, function () {
          $(this).remove();
        });
      });
    } else {
      // If the XHR failed, assume the replace command that would normally make
      // the styling disapear has also failed and remove the temporary styling.
      $('flag-waiting').toggleClass('flag-waiting');
    };
  };
})(jQuery, Drupal);
