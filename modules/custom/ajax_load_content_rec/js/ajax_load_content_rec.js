(function ($) {

    $(window).load(function () {

        $(".rec-home.rec.home").once('ajax_load_content_rec').click(function () {
        });

        /**
         * Leer el click en uno de los 3
         *
         * Saber a cual le dieron clic   *
         *
         * Cargar de 9 al que le dieron clic y de 1 a los otros dos
         *
         * Armar el markup con esas 3 vistas
         *
         * Pintar el markup
         *
         * ********  SEGUNDO PASO     Clic a los contraidos
         *
         *
         *
         *
         *
         *
         *
         *
         */

        var originalNames = [
            'review',
            'estreno',
            'cartelera'
        ];


        var ids9 = [
            'block_review_nueve_items',
            'block_estrenos_nueve_items',
            'block_cartelera_nueve_items'
        ];

        var ids1 = [
            'block_review_un_item',
            'block_estrenos_un_item',
            'block_cartelera_un_item'
        ];


        //Primer click
        $(".rec-home-widget-full").find(".containe-widget-home").on("click", function () {
            var dataid = $(this).find("div").data("contextual-id").split("_id=")[1];
            var id = dataid.split("&langcode=en")[0];
            //Cargamos la vista de 9
            load_ajax_views_render(id);
        });


        //Segundo click sobre una de 1 item
        $(".item-click-one").on("click", function () {


            var dataid = $(this).find("div.rec-home.rec.home").attr('class');
            //Cargamos la vista de 9
            load_ajax_views_render(dataid);
        });


        /**
         * @method Consulta las vistas, 1 param de  y las otras de 1
         * @paroriginalNamesam $idNine
         * @return {Array} las 3 vistas, en la primer posición la vista de 9
         */
        function load_ajax_views_render($idNine) {
            var names_to_render = [];

            for (var i = 0; i < originalNames.length; i++) {
                if ($idNine.includes(originalNames[i])) {
                    if (i == 2) {
                        names_to_render.push(0);
                        names_to_render.push(1);
                        names_to_render['9'] = ids9[i];
                    } else if (i == 1) {
                        names_to_render.push(0);
                        names_to_render.push(2);
                        names_to_render['9'] = ids9[i];
                    } else if (i == 0) {
                        names_to_render.push(1);
                        names_to_render.push(2);
                        names_to_render['9'] = ids9[i];
                    }
                }
            }


            var xhr_1 = $.ajax("get_content_rec/" + ids1[names_to_render[0]]);

            console.log("get_content_rec/" + ids1[names_to_render[0]]);
            console.log("get_content_rec/" + ids1[names_to_render[1]]);
            console.log("get_content_rec/" + names_to_render[9]);

            $.when(xhr_1).done(function (xhr_1) {
                var xhr_2 = $.ajax("get_content_rec/" + ids1[names_to_render[1]]);
                clean_containers();
                var data = $("<div/>").html(xhr_1.content).text();
                $(".container-rec-ajax-multiple").find(".item-1-1").html(data);
                $.when(xhr_2).done(function (xhr_2) {
                    var xhr_9 = $.ajax("get_content_rec/" + names_to_render[9]);
                    var data = $("<div/>").html(xhr_2.content).text();
                    $(".container-rec-ajax-multiple").find(".item-1-2").html(data);
                    // animation content
                    $('.container-de-1').animate({
                        top: "0px",
                        opacity: 1
                    }, 1500);
                    $.when(xhr_9).done(function (xhr_9) {
                        var data = $("<div/>").html(xhr_9.content).text();
                        $(".container-rec-ajax-multiple").find(".nine-items").html(data);
                        // animation
                        $('.nine-items header').delay(800).animate({
                            left: 0
                        },300);
                        $('.nine-items .views-row').each(function(index, domElement) {
                            $(this).delay(index * 500).animate({
                                opacity: 1,
                                left: 0
                            }, 500);
                        });
   
                    });
                });
            });

        }
        // animation close content
        var clean_containers = function () {
            $(".rec-home-widget-full > section:nth-child(1)").animate({
                left: "-50%",
                opacity: 0
            }, 300);
            $(".rec-home-widget-full > section:nth-child(2)").delay(150).animate({
                left: "-50%",
                opacity: 0
            }, 200);
           $(".rec-home-widget-full > section:nth-child(3)").delay(250).animate({
                left: "-50%",
                opacity: 0
            }, 100);        
            $(".rec-home-widget-full").delay(500).fadeOut(100);
            $(".container-rec-ajax-multiple").css({
                "height": "auto",
                "min-height": "100vh"
            });

            $(".nine-items").empty();
            $(".item-click-one.item-1-1").empty();
            $(".item-click-one.item-1-2").empty();
        }
    });
}(jQuery));





