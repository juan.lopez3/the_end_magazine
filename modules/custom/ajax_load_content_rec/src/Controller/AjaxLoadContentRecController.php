<?php

/**
 * @file
 * Contains Drupal\ajax_load_content_rec\Controller\AjaxLoadContentRecController
 */

namespace Drupal\ajax_load_content_rec\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for AjaxLoadContentRecController routes.
 */
class AjaxLoadContentRecController extends ControllerBase {

	/**
	 *
	 * @param type $id_view
	 * @return type
	 */
	public function get_content($id_view) {

		$foo = views_embed_view("reviews_home", $id_view);


		$d = \Drupal::service('renderer')->renderPlain($foo);

		return new JsonResponse(array('content' => htmlentities($d->__toString())));

		//return ["#markup" => render($foo)];
	}

}
