<?php

//SendEmail


/**
 * @file
 * Contains Drupal\parrilla_send_emails_on_state_change\Controller\SendEmail
 */

namespace Drupal\parrilla_send_emails_on_state_change\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use  \Drupal\user\Entity\User;


/**
 * Controller
 */
class SendEmail extends ControllerBase {


	/**
	 *
	 * @param type $id_video
	 * @return string
	 */
	public function sendEmail($stateFrom) {
		//public function sendEmail($stateFrom, $statesTo, $userOwner, $directorSection, $creatorsSection) {
		$this->getUsersBySection();
		return array('#markup' => 'UNA PRUEBA');
	}


	public function getUsersBySection($section = NULL, $rol = NULL) {
		//9 == peliculas

		$editores = [];
		$creadores = [];

		$ids = \Drupal::entityQuery('user')
			->condition('status', 1)
			->execute();
		$users = User::loadMultiple($ids);

		$creadores = $this->filterUsersByRole($users, 'creador');
		$editores = $this->filterUsersByRole($users, 'editor');

		kpr(array('creadores' => $creadores, 'editores' => $editores));
		if ($rol && $rol == 'creador') {
			return $creadores;
		} else {
			return $editores;
		}

	}


	public function filterUsersByRole($users, $role, $section = 28) {

		$editores = [];
		$creadores = [];

		foreach ($users as $user) {

			//Valida que tenga un rol
			if (!is_array($user->get('roles')->getValue())) continue;


			//Valida que tenga una sección
			if (!$user->get('field_secc')->getValue()) continue;

			//Validamos que tenga la seccion indicada
			foreach (reset($user->get('field_secc')->getValue()) as $sect) {
				if ((int)$sect == $section) {

					foreach ($user->get('roles')->getValue() as $rol) {
						switch ($rol['target_id']) {
							case 'creador':
								array_push($creadores, $user->get('mail')->value);
								break;

							case 'editor':
								array_push($editores, $user->get('mail')->value);
								break;
						}
					}
				};
			}


		}

		if ($role && $role == 'creador') {
			return $creadores;
		} else {
			return $editores;
		}

	}
}

