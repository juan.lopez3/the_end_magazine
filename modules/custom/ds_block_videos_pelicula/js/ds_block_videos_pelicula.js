(function ($) {
    'use strict';

    Drupal.behaviors.ds_block_videos_pelicula = {
        attach: function (context, settings) {

            var base_url = Drupal.url('');

            /**
             * Cuando se haga click sobre un contenedor de video por medio de ajax se llama
             * el despliegue full deel video y se inserta el markup en el div full-video
             *
             *
             */
            $("div.view-videos div.node--type-video.node--view-mode-teaser").on("click", function () {

                $("div.view-videos div.node--type-video.node--view-mode-teaser").removeClass('active');
                $(this).addClass('active');
                var imgUrl = base_url + 'modules/custom/ds_block_videos_pelicula/img/62160.gif';
                var id_video = $(this).attr("data-history-node-id");
                $.ajax({
                    method: "POST",
                    url: base_url + "/get_render_full_video/" + id_video,
                    dataType: "html",
                    success: function (data) {

                        $("#img-load-videos").remove();
                        $("div.view-videos .full-video").empty();
                        $("div.view-videos .full-video").html("<div><img id='img-load-videos' style='float: right;margin-right: 45%;' src=''></div>");
                        var result = $("div.view-videos .full-video").append(data).find('#block-zurb-foundation-content').html();
                        $("div.view-videos .full-video").html(result);
                    },
                    error: function (xhr, status) {
                        $("#img-load-videos").remove();
                        $("div.view-videos .full-video").empty();
                        $("div.view-videos .full-video").html("Lo sentimos, tenemos un problema!, intente de nuevo mas tarde");
                    },
                    beforeSend: function () {

                        $("div.view-videos .full-video").empty();
                        //pinta un gif de cargando mientras se trae los datos por ajax
                        $("div.view-videos .full-video").html("<div style='width: 100%;height: 200px;'><img id='img-load-videos' style='float: right;margin-right: 45%;' src=''></div>");
                        $("#img-load-videos").attr("src", imgUrl);
                    }
                });

                /**
                 * Se hace el slider de peliculas
                 */
                if ($(window).width() > 768) {
                    $('.view-videos .listado-videos .slides').slick({
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: true,
                    });
                }

            });

            /**
             * Cuando se de click en el video destacado de la pelicula
             * simular click en el listado de videos
             * para que se active el full de video
             */

            $(".field-name-dynamic-block-fieldnode-videos-destacados .views-field-field-videos div.node--type-video.node--view-mode-destacado").on("click", function () {
                var id = $(this).attr("data-history-node-id");
                $(".listado-videos.flexslider_videos").find("[data-history-node-id='" + id + "']").click();

                // scroll page

                var p = $("div#videos-anchor");
                var offset = p.offset();
                var y = $(window).scrollTop();  //your current y position on the page
                $(window).scrollTop(offset.top);

            });

            $(".field-name-dynamic-block-fieldnode-noticias-destacadas .views-field-field-noticias-peli div.node--type-articulo.node--view-mode-destacado").on("click", function () {
                var id = $(this).attr("data-history-node-id");

                // scroll page

                var p = $(".field-node--field-noticias-peli").find("[data-history-node-id='" + id + "']");
                var offset = p.offset();
                var y = $(window).scrollTop();  //your current y position on the page
                $(window).scrollTop(offset.top);
            });

        }
    };
}(jQuery));