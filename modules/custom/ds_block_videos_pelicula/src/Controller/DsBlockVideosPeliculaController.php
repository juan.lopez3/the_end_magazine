<?php

/**
 * @file
 * Contains Drupal\ds_block_videos_pelicula\Controller\DsBlockVideosPeliculaController
 */

namespace Drupal\ds_block_videos_pelicula\Controller;

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Controller routines for DsBlockVideosPeliculaController routes.
 */
class DsBlockVideosPeliculaController extends ControllerBase {

	/**
	 *
	 * @param type $id_video
	 * @return string
	 */
	public function render_full_video($id_video) {

		$node = Node::load($id_video);

		$entity_type = 'node';
		$view_mode = 'videos_playing_home';
		$view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
		$storage = \Drupal::entityTypeManager()->getStorage($entity_type);
		$build = $view_builder->view($node, $view_mode);
		$output = render($build);
		$response = new Response();
		$response->setContent($output);
		return $response;
	}

}
