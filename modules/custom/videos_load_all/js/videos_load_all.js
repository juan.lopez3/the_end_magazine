//Set value ignore the error
(function ($ = jQuery) {
    $(window).load(function () {
        
        var block = $("#videos-items-container");
        var releatedVideo;
        var player;
        var actualVideo;
        var videos = $("#videos-items-container .node--type-video");

        block.on("click", ".views-row .node--type-video", function (e) {
            actualVideo = $(this).parent('.views-row').index();
        })

        block.on("click", ".node--type-video img", function (e) {
            reset();
            var img = $(this);
            if (!$(this)) {

            } else {
                var splitted = $(this).attr('src').split("/");
                var dirtyId = splitted[splitted.length - 1];
                var vid = '';
                if (dirtyId.includes(".jpg")) {
                    vid = dirtyId.split(".jpg")[0];
                } else if (dirtyId.includes(".png")) {
                    vid = dirtyId.split(".png")[0];
                } else {
                    vid = dirtyId.split(".")[0];
                }
                var rest = loadSingleVideo(vid, e);
                $(".items-embebed-temp").remove();
                if ($(window).width() > 768) {
                    $(this).parents(".node--type-video").find(".group-left").addClass('big');
                    $(this).parents(".node--type-video").find(".group-right").addClass('big');
                }
                $(this).animate({
                    opacity: 0
                }, 100);
                $(this).parents(".node--type-video").find(".group-left").append(rest);
            }


        });

        block.on("click", "span#close-video-playing", function (e) {
            $(".items-embebed-temp").remove();
            reset();
        });


        var reset = function () {
            if ($(window).width() > 768) {
                 $(".node--type-video .group-left").removeClass('big');
                 $(".node--type-video .group-right").removeClass('big');
            }
            $("#videos-items-container .node--type-video .group-left img").animate({
                opacity: 1
            }, 50);
        }

        var loadSingleVideo = function (id, e) {
            var cargando_lib = false;
            if (cargando_lib != true) {
                // 2. This code loads the IFrame Player API code asynchronously.
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                cargado_libreria = true;
            }

            e.preventDefault();
            var URL = id || '6za0Ju8Ntp4';
            var htm = '<iframe id="player" width="425" height="349" src="http://www.youtube.com/embed/' + URL + '?enablejsapi=1&autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>';
            return '<div class="items-embebed-temp"><span id="close-video-playing">X</span><div class="item-tmp">' + htm + '</div></div>';
        }

        window.onYouTubePlayerAPIReady = function () {
            player = new YT.Player('player', {
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady() {
            console.log('youtube player is ready');
        }

        function onPlayerStateChange(e) {
            if (player.getPlayerState() === 0) {
                $(videos[actualVideo+1]).find('img').click();
                window.onYouTubePlayerAPIReady();
            }
        }
    });


})();