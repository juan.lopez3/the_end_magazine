<?php

//videos_load_all

namespace Drupal\videos_load_all\Controller;

use \Drupal\Core\Controller\ControllerBase;

class LoadAllVideos extends ControllerBase {

	public function LoadAllVideos() {

		$viewMode = 'video_thumb_interna_videos';

		$view_builder_video = \Drupal::entityTypeManager()->getViewBuilder('node', $viewMode);
		$returnItems = '<div id="videos-items-container">';
		$nids = \Drupal::entityQuery('node')
			->condition('type', 'video')
			->execute();

		$nodes = \Drupal::entityTypeManager()
			->getStorage('node')
			->loadMultiple($nids);

		foreach ($nodes as $node) {
			$nodeViwed = $view_builder_video->view($node, $viewMode);
			$nodeRendered = render($nodeViwed);
			$returnItems .= $nodeRendered;
		}
		$returnItems .= '</div>';

		return ["#markup" => "<h2>VIDEOS</h2>" . $returnItems];
	}

}