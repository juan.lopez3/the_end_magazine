/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $(document).ready(function () {

        $("div.link_seguir").click(function () {

            console.log(drupalSettings.user.uid);

            // si el usuario es anonimo se debe abrir el popup de login 
            if (drupalSettings.user.uid === 0) {

                $(".Login-popup-link a.use-ajax.login-popup-form").click();

            } else {

                var id_target = $(this).attr("id");

                // agregar tema al usuario actual
                if ($(this).hasClass("link_seguir_tema")) {
                    console.log("add");
                    var url = $(this).find('span').attr('data-href');
                    console.log(url);
                    $.ajax({
                        method: "POST",
                        url: url
                    }).done(function (data) {
                        if (data === true) {
                            $('div#' + id_target + '.link_seguir span').html("NO SEGUIR TEMA");
                            $('div#' + id_target + '.link_seguir').addClass("link_desactivar_seguir_tema").removeClass("link_seguir_tema");
                        } else {
                            $('div#' + id_target + '.link_seguir span').html("¡¡ ERROR !! SIGUIENDO TEMA");
                        }
                    });
                } else { // eliminar tema al usuario actual
                    console.log("remove");

                    var url = $(this).find('span').attr('data-href-remove');
                    console.log(url);
                    $.ajax({
                        method: "POST",
                        url: url
                    }).done(function (data) {
                        if (data === true) {
                            $('div#' + id_target + '.link_desactivar_seguir_tema span').html("SEGUIR TEMA");
                            $('div#' + id_target + '.link_desactivar_seguir_tema').addClass("link_seguir_tema").removeClass("link_desactivar_seguir_tema");
                        } else {
                            $('div#' + id_target + '.link_desactivar_seguir_tema span').html("¡¡ ERROR !! NO SEGUIR TEMA");
                        }
                    });
                }
            }

        });

    });
}(jQuery));