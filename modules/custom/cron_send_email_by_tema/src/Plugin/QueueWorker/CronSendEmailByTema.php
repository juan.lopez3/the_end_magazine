<?php

namespace Drupal\cron_send_email_by_tema\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;

// CUSTOM
use Drupal\cron_send_email_by_tema\Controller\ExecuteCronTemaToCurrentUserController;

/**
 * @QueueWorker(
 *   id = "cron_send_email_by_tema",
 *   title = @Translation("Cron - Enviar Email por Tema"),
 *   cron = {"time" = 60}
 * )
 */
class CronSendEmailByTema extends QueueWorkerBase {

    /**
     * {@inheritdoc}
     */
    public function processItem($data) {

        $target_id = reset($data);
        
        \Drupal::logger('cron_send_email_by_tema')->notice("cron_send_email_by_tema");
        
        ExecuteCronTemaToCurrentUserController::execute_tema_to_current_user($target_id);
    }



}
