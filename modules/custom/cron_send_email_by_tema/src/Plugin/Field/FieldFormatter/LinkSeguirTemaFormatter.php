<?php

namespace Drupal\cron_send_email_by_tema\Plugin\Field\FieldFormatter;

// FormatterBase class.
use Drupal\Core\Field\FormatterBase;
// FieldItemInterface
use Drupal\Core\Field\FieldItemListInterface;
// Url
use Drupal\Core\Url;
// User
use Drupal\user\Entity\User;

//define('API_KEY', 'AIzaSyBPPWgqN4LHm7PZ3f6E9amZH5Wez9nU3cQ');

/**
 * Plugin implementation of the 'cron_send_email_by_tema' formatter.
 *
 * @todo https://www.drupal.org/node/1829202 Merge into 'link' formatter once
 *   there is a #type like 'item' that can render a compound label and content
 *   outside of a form context.
 *
 * @FieldFormatter(
 *   id = "link_seguir_tema",
 *   label = @Translation("Link Seguir Tema"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class LinkSeguirTemaFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {

        $element = array();

        if (!empty($items->getValue())) {

            global $base_url;

            $valor = $items->getValue();
            $valor = reset($valor);
            $valor = $valor['target_id'];

            $url = Url::fromUri($base_url . "/add_tema_to_current_user/" . $valor);
            $url_remove = Url::fromUri($base_url . "/remove_tema_to_current_user/" . $valor);

            $current_ya_sigue_tema = self::get_current_ya_sigue_tema($valor);

            $label = 'SEGUIR TEMA';
            $class_div = 'link_seguir_tema';
            if ($current_ya_sigue_tema) {
                $label = 'NO SEGUIR TEMA';
                $class_div = 'link_desactivar_seguir_tema';
            }

            $element = [
                '#markup' => '<div id="seguir-'.$valor.'" class="link_seguir ' . $class_div . '">'
                . '<span class="" data-target-id="' . $valor . '" data-href="' . $url->getUri() . '" data-href-remove="' . $url_remove->getUri() . '">' . $label . '</span>'
                . '</div>',
                '#attached' => [
                    'library' => [
                        'cron_send_email_by_tema/cron_send_email_by_tema'
                    ],
                ],
            ];
        }

        return $element;
    }

    /**
     *
     * @param type $valor
     * @return type
     */
    private function get_current_ya_sigue_tema($valor) {
        $user = self::get_current_user();
        if ($user->isAuthenticated()) {

            $usuario = User::load($user->id());

            if ($usuario->hasField('field_tema_principales_siguiendo')) {
                $temas_principales = $usuario->get('field_tema_principales_siguiendo')->getValue();

                foreach ($temas_principales as $tema) {
                    if ($tema['target_id'] == $valor) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     *
     */
    protected function get_current_user() {
        $user = \Drupal::currentUser();
        return $user;
    }

}
