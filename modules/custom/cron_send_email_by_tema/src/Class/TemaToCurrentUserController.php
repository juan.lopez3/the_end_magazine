<?php

/**
 * @file
 * Contains Drupal\cron_send_email_by_tema\Controller\ExecuteCronTemaToCurrentUserController.
 */

namespace Drupal\cron_send_email_by_tema\Clase;

use Drupal\sends_emails\SendsEmails;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

$base = realpath(drupal_get_path("module", "sends_emails"));
require_once($base . "/inc/SendsEmails.php");

/**
 * 
 */
class TemaToCurrentUserController {

    /**
     * 
     * @param type $target_id
     * @return type
     */
    public static function execute_tema_to_current_user($target_id) {

        $entity_type = "node";
        $view_mode = "teaser";

        $nodes = self::get_nodes($target_id);

        $view_builder = \Drupal::entityManager()->getViewBuilder($entity_type);
        $output = "";

        foreach ($nodes as $node) {
            $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
            $build = $view_builder->view($node, $view_mode);
            $output .= render($build);
        }

        $emails = self::get_emails($target_id);

        $subject = "test";

        foreach ($emails as $mail) {
            if (SendsEmails::send_email_config($mail, $subject, $output)) {
                drupal_set_message("Correo enviado a " . $mail);
            } else {
                drupal_set_message("Error al enviar correo a " . $mail, "error");
            }
        }
    }

    /**
     * 
     * @param type $target_id
     * @return type
     */
    protected static function get_nodes($target_id) {
        $nids = \Drupal::entityQuery('node')
                ->condition('status', 1)
                ->condition('type', "pelicula")
                ->condition('field_tema_principal_pelicula', (int) $target_id)
                ->sort('nid', "ASC")
                ->execute();

        if (!empty($nids)) {

            foreach ($nids as $key => $nid) {
                $others_nids = \Drupal::entityQuery('node')
                        ->condition('status', 1)
                        ->condition('type', ["persona", "pelicula"], "<>")
                        ->condition('field_pelicula', (int) $nid)
                        ->sort('nid', "ASC")
                        ->execute();
            }

            $array = array_merge($nids, $others_nids);

            if (!empty($array)) {
                $nodes = Node::loadMultiple($array);
            } else {
                $nodes = Node::load($nids);
            }

            return $nodes;
        }

        return [];
    }

    /**
     * 
     * @param type $target_id
     */
    protected static function get_emails($target_id) {
        $uids = \Drupal::entityQuery('user')
                ->condition('field_tema_principales_siguiendo', (int) $target_id)
                ->sort('uid', "ASC")
                ->execute();

        $emails = [];
        if (!empty($uids)) {
            if (!empty($uids)) {
                $users = User::loadMultiple($uids);
            } else {
                $users = User::load($uids);
            }
            foreach ($users as $key => $user) {
                $emails[] = $user->getEmail();
            }
        }

        return $emails;
    }

}
