<?php

/**
 * @file
 * Contains Drupal\cron_send_email_by_tema\Controller\AddTemaToCurrentUserController.
 */

namespace Drupal\cron_send_email_by_tema\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * 
 */
class AddTemaToCurrentUserController extends ControllerBase {

    /**
     * 
     * @param type $target_id
     * @return type
     */
    public function add_tema_to_current_user($target_id) {

        $user = self::get_current_user();

        if ($user->isAuthenticated()) {

            $usuario = User::load($user->id());

            if ($usuario->hasField('field_tema_principales_siguiendo')) {
                $temas_principales = $usuario->get('field_tema_principales_siguiendo')->getValue();

                $temas = [];
                foreach ($temas_principales as $tema) {
                    $temas[] = $tema['target_id'];
                }
                $array = array_merge($temas, [$target_id]);
                $unique = array_unique($array);
                $usuario->set('field_tema_principales_siguiendo', $unique, false);

                ////

                if (!$usuario->save()) {
                    die("error guardando usuario " . realpath(__FILE__));
                    return JsonResponse::create(false);
                }
            }
        }

        return JsonResponse::create(true);
    }

    public function remove_tema_to_current_user($target_id) {

        $user = self::get_current_user();

        if ($user->isAuthenticated()) {

            $usuario = User::load($user->id());

            if ($usuario->hasField('field_tema_principales_siguiendo')) {
                $temas_principales = $usuario->get('field_tema_principales_siguiendo')->getValue();

                $temas = [];
                foreach ($temas_principales as $tema) {

                    if ($tema['target_id'] != $target_id)
                        $temas[] = $tema['target_id'];
                }

                $usuario->set('field_tema_principales_siguiendo', $temas, false);
                $usuario->set('name', "desarrollador");

                if (!$usuario->save()) {
                    die("error guardando usuario " . realpath(__FILE__));
                    return JsonResponse::create(false);
                }
            }
        }

        return JsonResponse::create(true);
    }

    /**
     * 
     */
    protected function get_current_user() {
        $user = \Drupal::currentUser();
        return $user;
    }

}
