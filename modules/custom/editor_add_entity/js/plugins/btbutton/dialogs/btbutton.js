/**
 * @file
 * Drupal Image plugin.
 *
 * This alters the existing CKEditor image2 widget plugin to:
 * - require a data-entity-type and a data-entity-uuid attribute (which Drupal
 *   uses to track where images are being used)
 * - use a Drupal-native dialog (that is in fact just an alterable Drupal form
 *   like any other) instead of CKEditor's own dialogs.
 *
 * @see Drupal\editor\Form\EditorImageDialog
 *
 * @ignore
 */

(function ($, Drupal, CKEDITOR) {

    'use strict';
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var url = baseUrl + "/get_editor_list_nodes";

    CKEDITOR.dialog.add('btbuttonDialog', function (editor) {
        return {
            title: 'Properties',
            minWidth: 400,
            minHeight: 200,
            contents: [{
                    id: 'general',
                    label: 'Basic Settings',
                    elements: [{
                            type: 'text',
                            id: 'btbutton',
                            label: 'Select'
//                                    items: [['France', 'FR'], ['Germany', 'DE']]
                        }, {
                            type: 'html',
                            label: 'Opciones',
                            html: '<datalist id="json-datalist"></datalist>'

                        }]
                }
            ],
            onShow: function () {

                jQuery("input#cke_58_textInput").attr('class', 'ajax');
                jQuery("input#cke_58_textInput").attr('list', 'json-datalist');
                jQuery("input#cke_58_textInput").attr('placeholder', 'e.g. The reverant');

                // Get the <datalist> and <input> elements.
                var dataList = document.getElementById('json-datalist');
                var input = document.getElementsByClassName('ajax');

                jQuery('input#cke_58_textInput').focus();
                jQuery("input#cke_58_textInput").bind('keypress', function (e) {

                    var valor = jQuery(this).val();

                    if (valor !== '') {

                        // Create a new XMLHttpRequest.
                        var request = new XMLHttpRequest();

                        // Handle state changes for the request.
                        request.onreadystatechange = function (response) {

                            if (request.readyState === 4) {
                                if (request.status === 200) {

                                    // Parse the JSON
                                    var jsonOptions = JSON.parse(request.responseText);

                                    // Loop over the JSON array.
                                    jsonOptions.forEach(function (item) {
                                        // Create a new <option> element.
                                        var option = document.createElement('option');
                                        // Set the value using the item in the JSON array.
                                        option.value = item;
                                        // Add the <option> element to the <datalist>.
                                        dataList.appendChild(option);
                                    });

                                    // Update the placeholder text.
                                    input.placeholder = "e.g. datalist";
                                } else {
                                    // An error occured :(
                                    input.placeholder = "Couldn't load datalist options :(";
                                }
                            }
                        };

                        // Update the placeholder text.
                        input.placeholder = "Loading options...";

                        // Set up and make the request.

                        console.log(url + "/" + valor);

                        request.open('GET', url + "/" + valor, true);
                        request.send();

                    }

                });
            },
            onOk: function () {
                var dialog = this;
                var btbutton = editor.document.createElement('div');
                var titulo = dialog.getValueOf('general', 'btbutton');
                var getUrl = window.location;
                var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                var url = baseUrl + "/get_render_node/" + titulo;
                btbutton.setAttribute('id', 'container_node_related');
                // ajax
                $.ajax({
                    method: "POST",
                    url: url
                }).done(function (data) {
                    btbutton.setHtml(data);
                    editor.insertElement(btbutton);
                });
            }
        };
//        });
    });


})(jQuery, Drupal, CKEDITOR);
