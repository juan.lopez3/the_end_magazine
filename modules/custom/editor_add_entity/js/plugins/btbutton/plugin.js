/**
 * @file
 * Drupal Image plugin.
 *
 * This alters the existing CKEditor image2 widget plugin to:
 * - require a data-entity-type and a data-entity-uuid attribute (which Drupal
 *   uses to track where images are being used)
 * - use a Drupal-native dialog (that is in fact just an alterable Drupal form
 *   like any other) instead of CKEditor's own dialogs.
 *
 * @see Drupal\editor\Form\EditorImageDialog
 *
 * @ignore
 */

(function ($, Drupal, CKEDITOR) {

    'use strict';

    CKEDITOR.plugins.add('btbutton', {
// Register the icons.
        icons: 'btbutton',
// The plugin initialization logic goes inside this method.
        init: function (editor) {

            // Define an editor command that opens our dialog.
            editor.addCommand('btbutton', new CKEDITOR.dialogCommand('btbuttonDialog'));

            // Create a toolbar button that executes the above command.
            editor.ui.addButton('btbutton', {
                // The text part of the button (if available) and tooptip.
                label: 'Insert Entity',
                // The command to execute on click.
                command: 'btbutton',
                // The button placement in the toolbar (toolbar group name).
                toolbar: 'source'
            });

            // Register our dialog file. this.path is the plugin folder path.
            CKEDITOR.dialog.add('btbuttonDialog', this.path + 'dialogs/btbutton.js');
        }
    });

})(jQuery, Drupal, CKEDITOR);

