<?php

namespace Drupal\editor_add_entity\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "btbutton" plugin.
 *
 * @CKEditorPlugin(
 *   id = "btbutton",
 *   label = @Translation("btbutton"),
 *   module = "ckeditor"
 * )
 */
class btbutton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return drupal_get_path('module', 'editor_add_entity') . '/js/plugins/btbutton/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        return array(
            'core/drupal.ajax',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        return array(
            'btbutton_dialogTitleAdd' => t('Add Entity'),
            'btbutton_dialogTitleEdit' => t('Edit Entity'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return array(
            'btbutton' => array(
                'label' => t('Add Entity'),
                'image' => drupal_get_path('module', 'editor_add_entity') . '/js/plugins/btbutton/icons/btbutton.png',
            ),
        );
    }

    /**
     * {@inheritdoc}
     *
     * @see Drupal\editor\Form\EditorImageDialog
     * @see editor_image_upload_settings_form()
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
        $form['type_contents'] = self::editor_add_entity_settings_form($editor);
        $form['type_contents']['#element_validate'][] = array($this, 'validateAddEntitySettings');
        return $form;
    }

    /**
     * #element_validate handler for the "image_upload" element in settingsForm().
     *
     * Moves the text editor's image upload settings from the DrupalImage plugin's
     * own settings into $editor->image_upload.
     *
     * @see Drupal\editor\Form\EditorImageDialog
     * @see editor_image_upload_settings_form()
     */
    function validateAddEntitySettings(array $element, FormStateInterface $form_state) {
        \Drupal::state()->set("config_editor_add_entity", json_encode($element['content_types']['#value']));
    }

    /**
     * FORMULARIO DE CONFIGURACION
     * @url admin/config/content/formats/manage/full_html
     * @param Editor $editor
     * @return type
     */
    protected static function editor_add_entity_settings_form(Editor $editor) {
        // Defaults.
        $default = (array) json_decode(\Drupal::state()->get("config_editor_add_entity"));

        $options = self::get_content_types();

        $form['content_types'] = array(
            '#type' => 'checkboxes',
            '#title' => t('Seleccionar'),
            '#options' => $options,
            '#default_value' => $default
        );

        return $form;
    }

    /**
     * 
     * @return type
     */
    protected static function get_content_types() {
        $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();

        $contentTypesList = [];
        foreach ($contentTypes as $contentType) {
            $contentTypesList[$contentType->id()] = $contentType->label();
        }

        return $contentTypesList;
    }

}

//322 2199182
//
//correo de distribuidor de cali