<?php

/**
 * @file
 * Contains Drupal\editor_add_entity\Controller\GetEditorListNodesController.
 */

namespace Drupal\editor_add_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;

/**
 * 
 */
class GetEditorListNodesController extends ControllerBase {

    /**
     * 
     * @param type $string
     * @return type
     */
    public function get_editor_list_nodes($string) {

        $config_editor_add_entity = (array) json_decode(\Drupal::state()->get("config_editor_add_entity"));

        $nodes = self::get_nodes($config_editor_add_entity, $string);

        $items = [];
        foreach ($nodes as $node) {
            $items[] = $node->getTitle();
        }

        return new JsonResponse($items);
    }

    /**
     * 
     * @param type $config_editor_add_entity
     * @param type $string
     * @return type
     */
    protected static function get_nodes($config_editor_add_entity, $string = null) {
        $query = \Drupal::entityQuery('node')
                ->condition('status', 1)
                //->condition('type', $config_editor_add_entity, 'IN')
                ->sort('nid', "ASC");

        if (!is_null($string))
            $query->condition('title', '%' . $string . '%', 'LIKE');

        $nids = $query->execute();

        if (!empty($nids)) {

            if (is_array($nids)) {
                $nodes = Node::loadMultiple($nids);
            } else {
                $nodes = Node::load($nids);
            }

            return $nodes;
        }

        return [];
    }

}
