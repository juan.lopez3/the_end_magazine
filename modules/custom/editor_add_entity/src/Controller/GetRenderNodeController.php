<?php

/**
 * @file
 * Contains Drupal\editor_add_entity\Controller\GetRenderNodeController.
 */

namespace Drupal\editor_add_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;

/**
 * 
 */
class GetRenderNodeController extends ControllerBase {

    /**
     * 
     * @param type $titulo
     * @return type
     */
    public function get_render_node($titulo) {

        $config_editor_add_entity = (array) json_decode(\Drupal::state()->get("config_editor_add_entity"));
        $id_node = self::get_id_node_by_title($config_editor_add_entity, $titulo);
        $view_mode = "contenido";

        if ($id_node) {
            $output = self::get_render_node_by_id($id_node, $view_mode);
            return JsonResponse::create($output);
        }
        return JsonResponse::create(false);
    }
    
    /**
     * 
     * @param type $id_node
     * @param type $view_mode
     * @param type $entity_type
     * @return string
     */
    public static function get_render_node_by_id($id_node, $view_mode, $entity_type = "node") {

        $node = Node::load($id_node);

        if ($node) {
            $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
            $build = $view_builder->view($node, $view_mode);
            $output = render($build);

            return $output;
        }
        return "";
    }

    /**
     * 
     * @param type $config_editor_add_entity
     * @param type $titulo
     * @return type
     */
    protected static function get_id_node_by_title($config_editor_add_entity, $titulo) {
        $query = \Drupal::entityQuery('node')
                ->condition('status', 1)
                //->condition('type', $config_editor_add_entity, 'IN')
                ->sort('nid', "ASC");

        if (!is_null($titulo))
            $query->condition('title', $titulo, '=');

        $nids = $query->execute();

        if (!empty($nids)) {
            return reset($nids);
        }

        return [];
    }

}
