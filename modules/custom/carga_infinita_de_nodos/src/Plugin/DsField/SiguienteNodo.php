<?php

namespace Drupal\carga_infinita_de_nodos\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

//Views

/**
 * Plugin that renders the field.
 *
 * @DsField(
 *   id = "siguiente_nodo",
 *   title = @Translation("Siguiente Nodo"),
 *   entity_type = "node",
 *   provider = "carga_infinita_de_nodos",
 *
 * )
 */
class SiguienteNodo extends DsFieldBase {
    /**
     * {@inheritdoc}
     */
    public function build() {

        $node = $this->entity();
        //Permite poner campo para cargar el siguiente nodo
        return array(
            '#markup' => '<a id="item-load-next-node" data-node-id="' . $node->nid->value . '">Cargar siguiente</a>'
        );
    }
}
