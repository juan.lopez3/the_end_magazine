<?php

namespace Drupal\carga_infinita_de_nodos\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;

//Views
/**
 * Plugin that renders the field.
 *
 * @DsField(
 *   id = "nodos_relacionados_pelicula",
 *   title = @Translation("Nodos Relacionados Película"),
 *   entity_type = "node",
 *   provider = "carga_infinita_de_nodos",
 *
 * )
 */
class InternaPelicula extends DsFieldBase {
	/**
	 * {@inheritdoc}
	 */
	public function build() {


		$node = $this->entity();

		//Cargar los argumentos a pasar a la vista
		$args = $node->nid->value;

		$display_id = 'block_1';

		if ($node->bundle() == "pelicula") {
			$output = views_embed_view('interna_pelicula', $display_id, $args);
		} elseif ($node->bundle() == "festival") {
			$output = views_embed_view('interna_festivales', $display_id, $args);
		}

		$node = \Drupal\node\Entity\Node::load($node->get('nid')->value);
		//Asignar la vista al campo


		return array(
			'#markup' => render($output)
		);
	}
}
