<?php

namespace Drupal\carga_infinita_de_nodos\Controller;


use Symfony\Component\HttpFoundation\Response;
use \Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class getNodesToLoad extends ControllerBase {

	public function getNodeNids($id) {
		$node = Node::load($id);
		$bundle = $node->bundle();
		$nids = \Drupal::entityQuery('node')
			->condition('status', 1)
			->condition('type', $bundle)
			->sort('nid', 'DESC')
			->execute();

		if (($key = array_search($id, $nids)) !== false) {
			unset($nids[$key]);
		}
		$nids = implode('-', $nids);
		$return = json_encode($nids, true);
		$response = New Response();
		$response->setContent($return);
		return $response;
	}

}