<?php

namespace Drupal\carga_infinita_de_nodos\Controller;

use Drupal\prev_next\PrevNextHelper;
use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\prev_next\PrevNextHelperInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class DefaultController.
 *
 * @package Drupal\carga_infinita_de_nodos\Controller
 */
class DefaultController extends ControllerBase {

	/**
	 * Hello.
	 *
	 * @return string
	 *   Return Hello string.
	 */
	public function hello($id) {
		//Cargar nodo quemado por el id de la ur
		$entity_type = 'node';
		if ($id == -1) {
			$output = "<div class='no-contenido'>No hay más contenido</div>";
		} else {
			$node = Node::load($id);
			$viewMode = ($node->get('ds_switch')->value) ? $node->get('ds_switch')->value : 'default';
			$view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
			$build = $view_builder->view($node, $viewMode);
			$output = render($build);
		}
		return ['#markup' => $output];

		$response = new Response();
		return $response->setContent($output);

		return $build;
	}

}

