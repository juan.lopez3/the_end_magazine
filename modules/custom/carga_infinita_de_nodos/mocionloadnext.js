jQuery(document).ready(function () {
    var $ = jQuery;

    /**
     * Se declaran variables globales
     */
    var nodeActual = $("#item-load-next-node").data("node-id"); //se toma el id del nodo que está en la url
    var nodeAnterior = nodeActual;
    var rendered = []; //los nodos que ya se han mostrado
    var nidsToRender = []; //los nodos a mostrar

    rendered.push(nodeActual); //guardamos el nodo actual por que ya lo mostramos

    /**
     * Conseguir los ids de los nodos apenas cargue la primer película
     */
    jQuery.get(Drupal.url("") + "carga_infinita_de_nodos/getNodesToLoad/" + nodeActual, function (data) {
        if (!data.includes("Fatal error")) {
            var parsed = JSON.parse(data);
            nidsToRender = parsed.split('-'); //Guardamos los nodos a mostrar en un arreglo
        }

    });


    /**
     * Detectamos el clic en el botón ver siguiente nodo
     */
    $('body').on('click', '#item-load-next-node', function (evt) {

        $('.field-node--siguiente-nodo').addClass('loading'); //Se pone clase loading

        if (nidsToRender.length != 0) {
            $nid = nidsToRender[nidsToRender.length - 1]; //El id del nodo que vamos a cargar
        } else {
            $nid = -1;
        }


        /**
         * Petición ajax para traer el contenido del siguiente nodo {$nid}
         */
        jQuery.get(Drupal.url("") + "carga_infinita_de_nodos/hello/" + $nid, function (data) {


            var success = $(data).find("#block-zurb-foundation-content").html(); //Contenido del nodo

            $("#block-zurb-foundation-content").append(success); //Se pinta el nuevo nodo

            $(evt.target).remove(); //Se elimina el botón al que dieron clic

            nodeAnterior = $("#item-load-next-node").data("node-id"); //Id del nodo que acabamos de cargar

            rendered.push(nodeAnterior); //Se guarda el id del nodo cargado

            var index = nidsToRender.indexOf($nid); //Toma el id del nodo que mostramos

            if (index > -1) {
                nidsToRender.splice(index, 1); //QUITA DEL ARREGLO DE LOS NODOS A MOSTRAR EL QUE YA MOSTRAMOS
            }


            /**
             * Recargar funciones
             */


            // menu sticky
            var sticky = $('.block-zurb-foundation-content > div').stickem();
            sticky.destroy();
            $('.block-zurb-foundation-content > div').stickem({
                item: '.menu-peli',
                container: '.block-zurb-foundation-content > div',
                offset: 100,
                start: 100
            });
            // $('.block-zurb-foundation-content > div').stickem({
            //     item: '.user-container-interna',
            //     container: '.block-zurb-foundation-content > div',
            //     offset: 400,
            //     start: 370  
            // });  


            lastMenu();

            formOrtigrafia();
            createSlick();
            removePopUp();
            if (checkIfIsPelucla()) {
                loadMovieRating();
            }
            loadVideoGrid();

        });
    });

    /**
     * Crear slider
     */
    function createSlick() {
        $(".cast-container .field-items").not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });

        $('.video-grid-container').not('.slick-initialized').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            slide: '.views-row',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
    }

    /**
     * Pone en la películas las barritas
     */
    function loadMovieRating() {
        $('.vote-form .form-select').barrating('show', {
            theme: 'bars-movie'
        })
    }

    /**
     * Quita el popup
     */
    function removePopUp() {
        var node = document.querySelector('.endPopUp');
        if (document.body.contains(node)) {
            var popUp = document.querySelector('.endPopUp');
            document.body.removeChild(popUp);
        }
    }

    function formOrtigrafia() {
        $('.errores-ortografia').css('display', 'none');
        $('.field-node--dynamic-block-fieldnode-syntax-error .text-syntax-error').on('click', function () {
            $('.errores-ortografia').toggle('swing');
        });
    }

    function checkIfIsPelucla() {
        var isPeli = false;
        var body = $("body");
        var container = body.find("#block-zurb-foundation-content");
        var containerNode = container.find(".contextual-region.node")
        var clasesNode = containerNode.attr('class').split(' ');
        for (var nodeClass in clasesNode) {
            if (isPeli) break;
            var item = clasesNode[nodeClass];
            if (item.includes('pelicula')) {
                isPeli = true;
            }
        }
        return isPeli;
    }

    /**
     * ajusta el menu al top cuando carga el nodo
     */
    function lastMenu() {
        $('.block-zurb-foundation-content > div:nth-last-child(2) .menu-peli.stickit-end').addClass('stickit');
        $('.block-zurb-foundation-content > div:nth-last-child(2) .menu-peli').removeClass('stickit-end');
        console.log('remove class');
    }
});
