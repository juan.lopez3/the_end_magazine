<?php

namespace Drupal\trabajeconnosotros\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class trabajeconnosotrosController.
 *
 * @package Drupal\trabajeconnosotros\Controller
 */
class trabajeconnosotrosController extends ControllerBase
{

  /**
   * Publiclink.
   *
   * @return string
   *   Return Hello string.
   */
    public function publiclink()
    {

        $host = \Drupal::request()->getSchemeAndHttpHost().base_path();
        $url =  $host."/form/trabaja-con-nosotros";
        if (\Drupal::currentUser()->isAnonymous()) {
            $url =  $host."/user/login";
            drupal_set_message(t('Debe ser usuario registrado para hacer la solicitud'), 'status', true);
            drupal_set_message(t('Si aún no se ha registrado o si ingresa por redes sociales,  deberá volver a pulsar sobre el enlace para continuar, luego de que termine su proceso de registro'), 'status', true);
            return $this->redirect('user.page');
        } else {
            $url =  $host."trabaja-con-nosotros";
            $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
            $response->send();
        }
        return;
    }
}
