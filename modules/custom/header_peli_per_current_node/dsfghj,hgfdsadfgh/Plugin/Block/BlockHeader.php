<?php


namespace Drupal\header_peli_per_current_node\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Class BlockHeader
 * @package Drupal\header_peli_per_current_node\Plugin\Block
 */


/**
 * Provides a 'Heder Peli' Block.
 *
 * @Block(
 *   id = "header_peli",
 *   admin_label = @Translation("Header pelicula internaxxx"),
 * )
 */
class BlockHeader extends BlockBase {

	/**
	 * 1 - Verificar que es la url donde cargan los contenidos (video)
	 *
	 * 2 - Guardar el tipo de contenido a buscar y el id de la película
	 *
	 * 3 - Cargar todos los nodos del tipo dado asociados a la película con el id guardado
	 *        Cargar la película con el display según el id guardado
	 *
	 * 4 - Llenar el bloque con el paso 3
	 *
	 */


	/**
	 * {@inheritdoc}
	 */
	public function build() {

		/**
		 * 1
		 */
    //\Drupal::service('page_cache_kill_switch')->trigger();
		$current_path = \Drupal::service('path.current')->getPath();
		$splitted = explode("/", $current_path);
		$filtered = array_filter($splitted);
		$ordered = array_values($filtered);

		/**
		 * 2
		 */
		$type = $ordered[0];
		$pid = $ordered[1];


		/**
		 * 3
		 */
		$node = Node::load($pid);

		if ($node && $node->type->entity->label() == 'Pelicula' && $type != 'node') {

			/**
			 * 4  Load header
			 */
			$view_builder_header = \Drupal::entityTypeManager()->getViewBuilder('node', "header_peli_in_related_content");
			$header = render($view_builder_header->view($node, "header_peli_in_related_content"));

			/**
			 * 4 Load items
			 */
			$ret = _get_related_movie($node, $type);
			if (count(reset($ret)) <= 0) return array();

			$items = reset($ret);

			//Add header n item
			$header .= "<div id='items-in-block'>" . $items . "</div>";


			//Render header n item
			return array(
				'#cache'=> ['max-age' => 0,],
				'#markup' => $header,
			);
		}

	}
}

/**
 * @mehtod Get the movie id who has referenced with the rid
 * @param $rid
 * @param $type
 * @return Array
 */
function _get_related_movie(&$node, $type) {
	$view_builder_nodo = \Drupal::entityTypeManager()->getViewBuilder('node');
	$ReturnItems = [];

	//Validate if the current node is type n return the movie id
	switch ($type) {
		case 'reviews':
			//dones
			$related = $node->get('field_reviews_peli')->getValue();
			if ($related) {
				$each = reset($related)['target_id'];
				if (is_array($each)) {
					foreach ($each as $item) {
						$nodeItem = Node::load($item);
						$nodeRendered = render($view_builder_nodo->view($nodeItem));
						array_push($ReturnItems, render($nodeRendered));
					}
				} else {
					$nodeItem = Node::load($each);
					if ($nodeItem) {
						$nodeRendered = render($view_builder_nodo->view($nodeItem));
						array_push($ReturnItems, render($nodeRendered));
					}
				}
			}
			break;
		case 'videos': //done
			$relateds = $node->get('field_videos')->getValue();
			$related = array_filter(reset($relateds));
			if ($related) {
				$each = $related['target_id'];
				if (is_array($each)) {
					foreach ($each as $item) {
						$nodeItem = Node::load($item);
						$view_builder_video = \Drupal::entityTypeManager()->getViewBuilder('node', 'video_thumb_interna_videos');
						$nodeRendered = render($view_builder_video->view($nodeItem, 'video_thumb_interna_videos'));
						array_push($ReturnItems, render($nodeRendered));
					}
				} else {
					$nodeItem = Node::load($each);
					if ($nodeItem) {
						$view_builder_video = \Drupal::entityTypeManager()->getViewBuilder('node', 'video_thumb_interna_videos');
						$nodeRendered = render($view_builder_video->view($nodeItem, 'video_thumb_interna_videos'));
						array_push($ReturnItems, render($nodeRendered));
					}
				}
			}
			break;
		case 'articulos':
			$related = $node->get('field_noticias_peli')->getValue();
			if ($related) {
				$each = reset($related)['target_id'];
				//kpr($each);
				if (is_array($each)) {
					foreach ($each as $item) {
						$nodeItem = Node::load($item);
						$nodeRendered = render($view_builder_nodo->view($nodeItem));
						array_push($ReturnItems, render($nodeRendered));
					}
				} else {
					$nodeItem = Node::load($each);
					if ($nodeItem) {
						$nodeRendered = render($view_builder_nodo->view($nodeItem));
						array_push($ReturnItems, render($nodeRendered));
					}
				}
			}
			break;
		case 'imagenes':
			$view_builder = \Drupal::entityTypeManager()->getViewBuilder('node', 'peli_just_images');
			$d = \Drupal::service('renderer')->renderPlain($view_builder->view($node, 'peli_just_images'));
			array_push($ReturnItems, render($d));
			//Render the images
			break;
	}
	return $ReturnItems;
}
