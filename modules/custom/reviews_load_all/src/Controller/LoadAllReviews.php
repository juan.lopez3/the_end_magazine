<?php

//reviews_load_all

namespace Drupal\reviews_load_all\Controller;

use \Drupal\Core\Controller\ControllerBase;

class LoadAllReviews extends ControllerBase {

	public function LoadAllReviews() {

		$mode = 'teaser';

		$view_builder_video = \Drupal::entityTypeManager()->getViewBuilder('node', $mode);
		$returnItems = '<div id="videos-items-container">';
		$nids = \Drupal::entityQuery('node')
			->condition('type', 'articulo')
			->execute();

		$nodes = \Drupal::entityTypeManager()
			->getStorage('node')
			->loadMultiple($nids);

		foreach ($nodes as $node) {

			$nodeType = $node->get('field_tipo_de_articulo')->getValue();
			$target = reset($nodeType);
			$taxRelated = $target['target_id'];

			if (isset($taxRelated) && $taxRelated == '21') {
				$nodeViwed = $view_builder_video->view($node, $mode);
				$nodeRendered = render($nodeViwed);
				$returnItems .= $nodeRendered;
			}

		}
		$returnItems .= '</div>';

		return ["#markup" => $returnItems];
	}

}